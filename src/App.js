import logo from './logo.svg';
import './App.css';
import {Link} from "react-router-dom"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Mau Kemana Gan?
        </p>
        <Link to="/about">
          <a className='App-link' href=''> Ke About Gan!</a>
        </Link>
        <Link to="/language">
          <a className='App-link' href=''> Ke Language Gan!</a>
        </Link>
      </header>
    </div>
  );
}

export default App;
