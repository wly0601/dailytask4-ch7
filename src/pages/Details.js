import '../App.css';
import Language from '../components/Language';
import {Link, useParams} from 'react-router-dom';

function Details(){
	let {nameParams} = useParams();
	const list = [
	{
	  name: 'HTML & CSS',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
	},
	{
	  name: 'JavaScript',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
	},
	{
	  name: 'React',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
	},
	{
	  name: 'Ruby',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
	},
	{
	  name: 'Ruby on Rails',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
	},
	{
	  name: 'Python',
	  alt:"",
	  image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
	}
  ];

  return (
		<div className="App">
			<header className="App-header">
			<h3> Language Name : {nameParams} </h3>
			{list.map((item) => {
				if(item.name == nameParams){
					return (
						<div style={{marginBottom : '30px'}}>
						<Language
							name={item.name}
							image={item.image}
						/>
						</div>
					)
				}
			})}
			<Link  to="/language">
				<a className='App-link' rel=""> Kembali ke Language Gan!</a>
			</Link>
			</header>
		</div>
	)
}

export default Details