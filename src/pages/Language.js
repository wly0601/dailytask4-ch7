import '../App.css';
import Language from '../components/Language';
import Header from '../components/Header.js';
import {Link} from "react-router-dom";

function language() {
  const languageList = [
    {
      name: 'HTML & CSS',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
    },
    {
      name: 'JavaScript',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
    },
    {
      name: 'React',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
    },
    {
      name: 'Ruby',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
    },
    {
      name: 'Ruby on Rails',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
    },
    {
      name: 'Python',
      alt:"",
      image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
    }
  ];

  return (
    <div className="App">
      <header className="App-header">
      <Header />
      <div className='language-grid'>
      {languageList.map((languageItem) => {
        return (
        <div style={{marginBottom:"25px"}}>
          <Language
            name={languageItem.name}
            image={languageItem.image}
          />
          <Link to={`/language/${languageItem.name}`}>
            <button className="App-button"> View! </button>
          </Link>
        </div>
        );
      })}
      </div>
        <p>
            Mau Kemana Lagi Gan?
        </p>
        <Link  to="/">
          <a className='App-link'> Ke Home Gan!</a>
        </Link>
        <Link  to="/about">
          <a className='App-link'> Ke About Gan!</a>
        </Link>
      </header>
    </div>
  );
}

export default language;
