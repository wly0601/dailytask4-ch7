import logo from '../logo.svg';
import '../App.css';
import {Link} from "react-router-dom";

function About() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Ini halaman About. Mau Kemana Lagi Gan?
        </p>
        <Link  to="/">
          <a className='App-link' rel=""> Ke Home Gan!</a>
        </Link>
        <Link  to="/language">
          <a className='App-link' rel=""> Ke Language Gan!</a>
        </Link>
      </header>
    </div>
  );
}

export default About;
